#BUILD THE IMAGE
docker build -t etsy dockerfile/

#RUN AT PORT 7272
docker run -i -t -p "7272:80" -v ${PWD}/../:/app -v ${PWD}/db/mysql:/var/lib/mysql --name etsy etsy:latest

#RE-RUN (container only after build)
docker container run -i -t -p "7272:80" -v ${PWD}/../:/app -v ${PWD}/db/mysql:/var/lib/mysql etsy:latest

#REMOVE CONTAINER
docker container rm [id container]