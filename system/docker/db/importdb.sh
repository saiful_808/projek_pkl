#!/usr/bin/env bash

# Simple bash script to help updating local database to the latest one
# by  : Kifni Taufik Darmawan

# Change this variable to match with your setting & account
DB_USER=admin
DB_PASS=admin

DB_NAME=etsy_db
CONTAINER_NAME=etsy

DB_VERSION=4

# # ssh to server
# SSH_USER=www-data
# SSH_SERVER=jagalab.com

# echo "Backup Database RUCVIEW to local server..."
# DB_FILE=`ssh $SSH_USER@$SSH_SERVER 'ls -1r /home/database_backup/ruc_view/ | head -1'`
# echo "Latest file $DB_FILE"

# DB_PATH=/home/database_backup/ruc_view/$DB_FILE

# if [ ! -d "dump" ]; then
#     mkdir dump
# fi

# echo "Downloading database backup from server..."
# scp $SSH_USER@$SSH_SERVER:$DB_PATH dump/$DB_FILE

cd ../../dev/mysql

echo "DROP AND CREATE DATABASE $DB_NAME"
docker exec -i $CONTAINER_NAME mysql -u $DB_USER -p$DB_PASS -e "DROP DATABASE IF EXISTS $DB_NAME;CREATE database $DB_NAME;"

echo "IMPORT DATABASE..."
docker exec -i $CONTAINER_NAME mysql -u $DB_USER -p$DB_PASS $DB_NAME < "web_analytic_$DB_VERSION.sql"
# pv $DB_FILE | gunzip | docker exec -i $CONTAINER_NAME mysql -u $DB_USER -p$DB_PASS $DB_NAME

echo "FINISHED. HAVE A NICE DAY !"

