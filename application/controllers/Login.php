
<?php
class Login extends CI_Controller{

  function __construct(){
		parent::__construct();
		$this->load->model('M_Shop');
		$this->load->library('form_validation');
  }

	public function index(){
		$this->load->view('login');
	}

	public function login(){
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
      
      	$cek = $this->M_Shop->get_where('user_web', array('username' => $user));

      	if ($cek->num_rows() > 0) {
      		$data = $cek->row();
      		if (password_verify($pass, $data->password)){
      			$datauser = array(
      				'admin' => $data->id,
      				'user'	=> $data->username,
      				'level'	=> $data->level,
      				'login'	=> TRUE
      			);
      			
      			$this->session->set_userdata($datauser);
      			redirect('Shop');
      		}else{
      			$this->session->set_flashdata('pass_salah', '<script>alert("maaf kamu password salah")</script>');
      			redirect('login');
      		}
	      	}else{
	  			$this->session->set_flashdata('wrong', '<script>alert("Invalid User")</script>');
	  			redirect('login');
	  		}

			if ($this->session->userdata('login') == TRUE) {
				redirect('Shop');
			}

			$this->load->view('login');
    }

  public function logout(){
    $this->session->sess_destroy();
    redirect('login');
  }

}
