
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shop extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('template','pagination');
		$this->load->model('M_Shop');
		$this->load->model('M_Toko');
		$this->load->helper('url');
	}
// Code For Checking User Already Login To Access The website
// This Code Putting in every Public function
$this->M_shop->cek_login();
/////////////////////////////////////////////////////////////


	public function index(){
			$data['data']=$this->M_Toko->twenty();
			$data['link']=$this->M_Toko->top();
			$this->template->shop('home',$data);
	}

	function data_toko(){
		$data=$this->M_Toko->data_toko();
		echo json_encode($data);
	}

	public function detail($id,$offset=0){
		$config['total_rows']=$this->M_Toko->count($id);
		$config['base_url']= base_url()."index.php/Shop/detail/$id/";
		$config['per_page']=12;

		// style paginationnya
		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

   	$this->pagination->initialize($config);
		$data['halaman']= $this->pagination->create_links();
		$data['offset']=$offset;

    $data['product']=$this->M_Toko->produk($id,$config['per_page'],$offset);
		$data['toko']=$this->M_Toko->all_data($id);
		// $data['product']=$this->M_Toko->produk($id);
		$this->template->shop('isi/Detail_p',$data);
	}

	public function isi($id){
		$data['produk']=$this->M_Toko->coment($id);
		$this->template->shop('isi/isi',$data);
	}

	public function admin(){
		$this->template->shop('admin/index');
	}

	public function cari($offset=0){
		$data['cariberdasarkan']=$this->input->post('cariberdasarkan');
		$data['yangdicari']=$this->input->post('yangdicari');
		$data['data']=$this->M_Shop->cari($data['cariberdasarkan'],$data['yangdicari'])->result();
		$data['jumlah']=count($data['data']);
		$this->template->shop('isi/cari',$data);
	}
	function fetch(){
		$output='';
		$query='';
		if ($this->input->post('query')) {
			$this->input->post('query');
		}
		$data =$this->M_Toko->fetch_data($query);
		$output .='
					<tr>
						<th>Nama</td>
					</tr>
		';
		if ($data->num_rows() > 0) 
		{
			foreach ($data->result() as $row ) {
					$output.='<tr>
									<td>'.$row->Id.'</td>
							  </tr>';
				}	
		}
		else
		{
			$output .= '<h1>No data found </h1>';
		}
		echo $output;
	}
	function get_autocomplete(){
		if (isset($_GET['term'])) {
			$result =$this->M_Toko->autocomplete($_GET['term']);
			if (count($result > 0)) {
				foreach ($result as $row ) {
					$arr_result =$row->Id;
					echo json_encode($arr_result);
				}
			}
		}
	}

}
