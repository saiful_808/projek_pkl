
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('M_Shop');
		$this->load->helper('url');
	}

// Code For Checking User Already Login To Access The website
	// This Code Putting in every Public function
	$this->M_shop->cek_login();
/////////////////////////////////////////////////////////////

public function index(){
  $data['data']= $this->M_Shop->get_all('user_web');

	if($this->session->userdata('level')=='2'){
		$this->template->shop('admin/index', $data);
	}else{
		$this->cekal();
	}

}

public function management_admin()
{
  $data['data']=$this->M_Shop->get_where_admin('user_web');
	if($this->session->userdata('level')=='2'){
		$this->template->shop('admin/index', $data);
	}else{
		$this->cekal();
	}

}

public function management_user()
{
  $data['data']=$this->M_Shop->get_where_user('user_web');

	if($this->session->userdata('level')=='2'){
		$this->template->shop('admin/index', $data);
	}else{
		$this->cekal();
	}

}

function cekal(){
	if ($this->session->userdata('level') == '1') {
		$this->load->view('admin/203');
	}
}

}
