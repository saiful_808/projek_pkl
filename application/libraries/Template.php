<?php

class Template {
	protected $_ci;

	function __construct()
	{
		$this->_ci =&get_instance();
		}

	function shop($template,$data=null)
	{
		$data['content']	=	$this->_ci->load->view($template,$data, TRUE);
		$data['footer']		=	$this->_ci->load->view('footer',$data, TRUE);
		$data['nav']		=	$this->_ci->load->view('nav',$data, TRUE);

		$this->_ci->load->view('index',$data);

	}

}
