<!DOCTYPE HTML>
	<html lang="en">
	<head>
		<title>Web Analytic | home</title>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		<link href="<?= base_url();?>admin_assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= base_url();?>admin_assets/css/styles.css" rel="stylesheet">
		<link href="<?= base_url();?>admin_assets/css/style.css" rel="stylesheet">
		<link href="<?= base_url();?>admin_assets/fonts/ionicons.css" rel="stylesheet">

		<!-- Data tables -->
	 	<link href="<?= base_url(); ?>admin_assets/css/responsive.bootstrap.min.css" rel="stylesheet">
  	 	<link href="<?= base_url(); ?>admin_assets/DataTable/datatables.min.css" rel="stylesheet">

		<!-- sweetalert -->
		<link rel="stylesheet" href="<?=base_url();?>admin_assets/sweetalert/dist/sweetalert.css">
		<!-- Custom stlylesheet -->
		<link rel="stylesheet" href="<?= base_url();?>admin_assets/css/style.css">


		<body>
		<?= $nav;  ?>

		<?= $content;  ?>
		<?= $footer;  ?>


			<!-- SCIPTS -->
			<script src="<?=base_url();?>admin_assets/js/jquery-3.2.1.min.js"></script>
			<script src="<?=base_url();?>admin_assets/js/tether.min.js"></script>
			<script src="<?=base_url();?>admin_assets/js/bootstrap.js"></script>
			<script src="<?=base_url();?>admin_assets/js/scripts.js"></script>
			<script src="<?= base_url();?>admin_assets/js/main.js"></script>
			<script src="<?= base_url();?>admin_assets/js/nav.js" charset="utf-8"></script>
			<!-- sweetalert -->
			<script src="<?=base_url();?>admin_assets/sweetalert/dist/sweetalert.min.js" charset="utf-8"></script>
			<script src="<?=base_url();?>admin_assets/sweetalert/dist/sweetalert.js" charset="utf-8"></script>
			<script src="<?=base_url();?>admin_assets/DataTable/datatables.min.js"></script>	
			<!-- top 20 -->
			<!-- /top 20 -->
			<!-- main js -->

			</script>
		<!-- Manual Script -->
		<script type="text/javascript">
			$(document).ready(function(){
				$('#p_table tfoot th').each(function(){
					var title =$(this).text();
					$(this).html('<input type="text" style="width:100px;" palceholder="search'+title+'"/>');
				});
				var table=$('#p_table').DataTable({
					 "scrollY":400,
					 "scrollX":true	,
					 "responsive": false,
					"columnDefs": [
		            { responsivePriority: 1, targets: 0 },
		            { responsivePriority: 2, targets: 4 }
		        ]
				});
				table.columns().every(function (){
					var that =this;
					$('input',this.footer()).on('keyup change ',function(){
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					})
				})
			});
		</script>
		<script type="text/javascript">

 		$(document).ready(function(){
	        tampil_data_barang();   //pemanggilan fungsi tampil barang.z

	        $('#mydata').dataTable();

	        //fungsi tampil barang
	        function tampil_data_barang(){
	            $.ajax({
	                type  : 'GET',
	                url   : '<?php echo base_url()?>index.php/Shop/data_toko',
	                async : false,
	                dataType : 'json',
	                success : function(data){
	                    var html = '';
	                    var i;
	                    for(i=0; i<data.length; i++ ){
	                        html += '<tr>'+
	                                '<td>'+data[i].No+'</td>'+
	                                '<td>'+data[i].Id+'</td>'+
	                                '<td>'+data[i].Join_since+'</td>'+
	                                '<td><a href="<?php echo base_url();?>index.php/Shop/Detail/'+data[i].Id+'" class="btn tombol"><i class="fa fa-search-plus"></i> view</a></td>'+
	                                '</tr>';
	                    }
	                    $('#show_data').html(html);
	                }

	            });
	        }

	    });
</script>
	</body>
	</html>
<!-- <script type="text/javascript">
			$(document).ready(function(){
				$('#p_table tfoot th').each(function(){
					var title =$(this).text();
					$(this).html('<input type="text" style="width:100px;" palceholder="search'+title+'"/>');
				});
				var table=$('#p_table').DataTable({
					 "scrollY":400,
					 "scrollX":true	,
					 "responsive": true,
					"columnDefs": [
		            { responsivePriority: 1, targets: 0 },
		            { responsivePriority: 2, targets: 4 }
		        ]
				});
				table.columns().every(function (){
					var that =this;
					$('input',this.footer()).on('keyup change ',function(){
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					})
				})
			});
		</script>
		<script type="text/javascript">

 		$(document).ready(function(){
	        tampil_data_barang();   //pemanggilan fungsi tampil barang.z

	        $('#mydata').dataTable();

	        //fungsi tampil barang
	        function tampil_data_barang(){
	            $.ajax({
	                type  : 'GET',
	                url   : '<?php echo base_url()?>index.php/Shop/data_toko',
	                async : false,
	                dataType : 'json',
	                success : function(data){
	                    var html = '';
	                    var i;
	                    for(i=0; i<data.length; i++ ){
	                        html += '<tr>'+
	                                '<td>'+data[i].No+'</td>'+
	                                '<td>'+data[i].Id+'</td>'+
	                                '<td>'+data[i].Join_since+'</td>'+
	                                '<td><a href="<?php echo base_url();?>index.php/Shop/Detail/'+data[i].Id+'" class="btn tombol"><i class="fa fa-search-plus"></i> view</a></td>'+
	                                '</tr>';
	                    }
	                    $('#show_data').html(html);
	                }

	            });
	        }

	    });
</script> -->