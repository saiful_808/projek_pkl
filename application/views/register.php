<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/main.css">
</head>

<body style="background-color: #555555;">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form" action="" method="post">
					<span class="login100-form-title p-b-43">
						Signup to continue
					</span>
          <?= validation_errors('<p style="color:red">', '</p>'); ?>
          <div class="validate">
            <input class="wrap-input100" type="text" id="nama" name="nama" class="form-control"
              placeholder="Nama" value="<?=$nama;?>" required autofocus>
          </div>

          <div class="validate">
            <input class="wrap-input100" type="text" id="username" name="username" class="form-control"
              placeholder="Username" value="<?=$username;?>" required >
          </div>

          <div class="validate">
            <input class="wrap-input100" type="email" id="email" name="email" class="form-control"
              placeholder="Email" value="<?= $email;  ?>" required >
          </div>

          <div class="validate">
						<input class="wrap-input100" type="password" id="password" name="password1" class="form-control"
							placeholder="Password" required >
					</div>

					<div class="validate">
						<input class="wrap-input100" type="password" id="password" name="password2" class="form-control"
							placeholder="Confirm Password" required>
					</div>

          <div class="flex-sb-m w-full p-t-3 p-b-32">
						<div>
							<a href="#" class="txt1">
								Forgot Password?
							</a>
						</div>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" style="background-color:#F9B500;" value="Submit" type="submit">
							Signup
						</button>
					</div>
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
              <a href="<?= base_url();?>login">Login</a>
				    </span>
					</div>
				</form>
				<div class="login100-more" style="background-image:url('admin_assets/css/bg-01.jpg')">
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url();?>admin_assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?= base_url();?>admin_assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url();?>admin_assets/login/js/main.js"></script>
</body>
</html>
