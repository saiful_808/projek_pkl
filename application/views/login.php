
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/sweetalert/dist/sweetalert.css">
</head>
<body style="background-color: #555555;">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form" action="<?= base_url().'login/auth_user'?>" method="post">
					<span class="login100-form-title p-b-43">
						Login to continue
					</span>
            <?php echo $this->session->flashdata('msg');?>
					<div class="validate-input">
						<input class="wrap-input100" type="text" id="username" name="username" class="form-control"
							placeholder="Username" required autofocus>
					</div>
					<div class="validate-input">
						<input class="wrap-input100" type="password" id="password" name="password" class="form-control"
							placeholder="Password" required>
					</div>
					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div>
							<a class="txt1" onclick="sweet();">
								Forgot Password?
							</a>
						</div>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" style="background-color:#F9B500;" type="submit" value="Submit" name="submit">
							Login
						</button>
					</div>
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
              <a onclick="sweet();">sign up</a>
				    </span>
					</div>
				</form>
				<div class="login100-more" style="background-image: url('admin_assets/login/images/bg-01.jpg')">
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	function sweet(){
		swal("Maaf","Dalam Masa Pembuatan","warning");
	}
</script>
	<script src="<?= base_url();?>admin_assets/sweetalert/dist/sweetalert.min.js" charset="utf-8"></script>
	<script src="<?= base_url();?>admin_assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?= base_url();?>admin_assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url();?>admin_assets/login/js/main.js"></script>
	<script src="<?= base_url(); ?>admin_assets/js/jquery.min.js"></script>


</body>
</html>

