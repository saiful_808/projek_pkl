<style type="text/css">
body{

}
.product.product-single {
  margin-top: 15px;
  margin-bottom: 15px;
  -webkit-transition: 0.3s all;
  transition: 0.3s all;
}

.product.product-single:hover {
  -webkit-box-shadow: 0px 6px 10px -6px rgba(0, 0, 0, 0.175);
  box-shadow: 0px 6px 10px -6px rgba(0, 0, 0, 0.175);
  -webkit-transform: translateY(-4px);
  -ms-transform: translateY(-4px);
  transform: translateY(-4px);
}

.product.product-single .product-btns {
  margin-top: 20px;
  opacity: 0;
  visibility: hidden;
  -webkit-transition: 0.3s all;
  transition: 0.3s all;
}

.product.product-single .product-btns .tombol{
 -webkit-transition: 0.3s all;
  background-color: #F9B500;
}
.product.product-single:hover .product-btns {
  opacity: 1;
  visibility: visible;
}
.tombol{
	background-color: #F9B500;
}
@media(min-width: 992px){
	.produk{
		background-color: #faf9f5;
		padding-top: 3%;
		padding-bottom: 3%;
	}
	.top{
		padding: 20px;
	}
	.gambar{
		margin-left: 8%;
		width: 300px;
		height: 200px;
	}
	.des{
		margin-top: 3%;
		float: right;
	}
	.p_img{
		width: 80px;
	}
}
</style>

<div class="container top shadow-sm p-3 mb-5 bg-white rounded">
	<img src="<?= $toko->Thumbnail;?>" alt="" class="gambar shadow-sm mb-5">
	<div class="des">
		<h3><?= $toko->Id;?></h3>
		<p><b>Total revenue :</b>$<?php echo number_format("$toko->total_revenue",2,",",".")?></p>
		<p>Join On Etsy <?= $toko->Join_since;?></p>
			<ul class="list-li-mr-20 mtb-15">
						<li>
						<i class="color-primary mr-5 font-12 fas fa-shopping-bag"></i>Total product <?= $toko->total_produk;?>
						</li> 
						<li>
							<i class="color-primary mr-5 font-17 fa fa-shopping-cart"></i>Sold <?= $toko->total_sales?>
						</li>
						<li>
							<i class="color-primary mr-5 font-17 ion-chatbubbles"></i>30
						</li>
					</ul>
					</div>
					</div>
					<section>
					<div class="produk">
					<div class="container">
					<h4 class="p-title mt-50">
						<b><i class="fas fa-list-ul"></i> Produk
						</b>
					</h4>
					 <table class="table table-striped" id="p_table">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Id</th>
                                      <th>Name</th>
                                      <th>Price</th>
                                      <th>Sold</th>
                                      <th>sales</th>
                                      <th>Income</th>
                                      <th>Coment</th>
                                      <th>Thumbnail</th>
                                      <th>Tindakan</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  	 <?php $i=1; foreach ($product as $dta) :?>
								 		<tr>
								 			<td><?= $i++ ?></td>
								 			<td> <?= $dta->id;?></td>
								 			<td><?php 
							                    $title=$dta->title;
							                     echo substr($title,0,3);?>           	
                   						  </td>
								 			<td>$ <?= $dta->price;?></td>
								 			<td><?= $dta->sold;?></td>
								 			<td><?= $dta->total_sales;?></td>
								 			<td>
								 				$<?php echo number_format("$dta->total_revenue_p",2,",",".");?>
								 			</td>
								 			<td>
								 				<?= $dta->coment ?>
								 			</td>
								 			<td> <img src="<?= $dta->thumbnail;?>" class="p_img"  alt="..."></td>
								 			<td> 
								 				<a href="<?php echo site_url("Shop/isi/").$dta->id."/".$toko->Id;?>" class="btn tombol"><i class="fa fa-search-plus"></i> view</a></td>
								 		</tr>
				                    <?php endforeach?>
                                  </tbody>
                                  <tfoot>
                                  	<tr>
                                      <th>No</th>
                                      <th>Id</th>
                                      <th>Name</th>
                                      <th>Price</th>
                                      <th>Sold</th>
                                      <th>sales</th>
                                      <th>Income</th>
                                      <th>Coment</th>
                                      <th>Thumbnail</th>
                                      <th>Tindakan</th>
                                    </tr>
                                  </tfoot>
                                </table>
                  			  </div>
							</div>

						</div>
						<div class="d-none d-md-block d-lg-none col-md-3"></div>
							</div>
							</div>
							</div>
						</section>
						</div>



