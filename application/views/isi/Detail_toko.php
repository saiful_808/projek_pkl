
<style type="text/css">
	.gambar_toko{
		width: 100%;
		height:auto;
	}
	@media(min-width: 992px){
	.gambar_toko{
		width: 400px;
		height:400px;
		margin-left: 110px;
	}
	}
</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-8">
				<img src="
					<?= $toko->Thumbnail;?>" alt="" class="gambar_toko img-thumbnail">
					<h3 class="mt-30">
						<b></b>
					</h3>
					<ul class="list-li-mr-20 mtb-15">
						<li>
							<i class="color-primary mr-5 font-17 fa fa-shopping-cart"></i><?= $toko->total_sales?>
						</li>
						<li>
							<i class="color-primary mr-5 font-17 ion-chatbubbles"></i>30
						</li>
					</ul>
					<h3><?= $toko->Id;?></h3>
					<p class="mtb-15">On Etsy Since <?= $toko->Join_since;?></p>
					<p class="mtb-15">Total Shop Revenue : <?= $toko->total_revenue;?></p>
					<div class="float-left-right text-center mt-40 mt-sm-20">
						<ul class="mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
							<li class="mr-10 ml-0">Share</li>
							<li>
								<a href="#">
									<i class="ion-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-google"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-instagram"></i>
								</a>
							</li>
						</ul>
					</div>
					<!-- float-left-right -->
					<div class="brdr-ash-1 opacty-5"></div>
					<h4 class="p-title mt-50">
						<b>Product</b>
					</h4>
						<!-- content -->
						<div class="row">
						   <?php foreach ($product as $dta) :?>
							<div class="col-sm-6 ">
							<div class="card" style="width: 18rem;">
							 <img src="<?= $dta->thumbnail;?>" class="card-img-top" alt="...">
							  <div class="card-body">
							   <p class="card-text"><?php substr($dta->title,10);?></p>
							   <p><i class="fas fa-money-check-alt"></i> $ <?= $dta->price;?></p>
							   <ul class="list-li-mr-20 pt-10 mb-30">
										<li>
											<i class="color-primary mr-5 font-12 fas fa-shopping-cart"></i> $ <?= $dta->total_revenue_p;?>
										</li>
										<li>
											<i class="color-primary mr-5 font-12 fas fa-shopping-bag"></i> <?= $dta->sold;?> pcs
										</li>
										<li>
											<i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47
										</li>
									</ul>
							  </div>
							</div>
							</div>
						<?php endforeach;?>
								<!-- col-sm-6 -->
							</div>
								<!-- col-sm-6 -->
						<!-- end content -->
							<!-- row -->
							<h4 class="p-title mt-20">
								<b>01 REVIEW</b>
							</h4>
							<div class="sided-70 mb-50">
								<div class="s-left rounded">
									<img src="<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
									</div>
									<!-- s-left -->
									<div class="s-right ml-100 ml-xs-85">
										<h5>
											<b>USER NAME, </b>
											<span class="font-8 color-ash">Nov 21, 2017</span>
										</h5>
										<p class="mt-10 mb-15">MANTAP GAN!!!.</p>
									</div>
									<!-- s-right -->
								</div>
								<!-- sided-70 -->
								<h4 class="p-title mt-20">
									<b>LEAVE A REVIEW</b>
								</h4>
								<form class="form-block form-plr-15 form-h-45 form-mb-20 form-brdr-lite-white mb-md-50">
									<input type="text" placeholder="Your Name*:">
										<input type="text" placeholder="Your Email*:">
											<input type="text" placeholder="Your Phone*:">
												<textarea class="ptb-10" placeholder="Your Comment"></textarea>
												<button class="btn-fill-primary plr-30" rows="4" cols="50" type="submit">
													<b>LEAVE A COMMENT</b>
												</button>
											</form>
										</div>
										<!-- col-md-9 -->
										<div class="d-none d-md-block d-lg-none col-md-3"></div>
										<div class="col-md-6 col-lg-4">
											<div class="">
												<h4 class="p-title">
													<b>Popular Produk</b>
												</h4>
												<a class="oflow-hidden pos-relative mb-20 dplay-block" href="#">
													<div class="row">
														<div class="card ml-2" style="width: 10rem;">
														  <img src="<?= base_url();?>assets/upload/ujan.jpg" class="card-img-top" alt="...">
														  <div class="card-body">
														    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
														  </div>
														</div>
														<div class="card ml-2" style="width: 10rem;">
														  <img src="<?= base_url();?>assets/upload/ujan.jpg" class="card-img-top" alt="...">
														  <div class="card-body">
														    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
														  </div>
														</div>
														</div>
													</a>
													<!-- oflow-hidden -->
												</div>
												<!-- mtb-50 -->
											</div>
										</div>
									</div>
								</div>
							</section>
=======
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-8">
				<img src="
					<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
					<h3 class="mt-30">
						<b></b>
					</h3>
					<ul class="list-li-mr-20 mtb-15">
						<li>
							<i class="color-primary mr-5 font-12 fa fa-shopping-cart"></i>30
						</li>
						<li>
							<i class="color-primary mr-5 font-12 ion-chatbubbles"></i>30
						</li>
					</ul>
					<p>Sifull DI grebeg warga.</p>
					<p class="mtb-15">Formerly known as Rootstock, the startup has long been lauded for Rs potential to pave the way for the
						implementation of ethereum-style smart contracts on broom. something enthusiasts believe vein keep the wodd's
						largest cryptocurrency competitive with the platform t hnl acquably pionverecl
						the idea I hat mote executing code could be rim on a bickchain.</p>
					<p class="mtb-15">But while it would be easy enough for Wan users that want more complex smart contracts oo merely
						some users believe that. as tdcoin is the largest and most secure cryptocumency, more experimental
						features that debut on other networks will eventually make hal in
						doing to, Uxy can r apitalier impressive startup infrastructure and serve different users.</p>
					<div class="float-left-right text-center mt-40 mt-sm-20">
						<ul class="mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
							<li class="mr-10 ml-0">Share</li>
							<li>
								<a href="#">
									<i class="ion-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-google"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="ion-social-instagram"></i>
								</a>
							</li>
						</ul>
					</div>
					<!-- float-left-right -->
					<div class="brdr-ash-1 opacty-5"></div>
					<h4 class="p-title mt-50">
						<b>YOU MAY ALSO LIKE</b>
					</h4>
					<div class="row">
						<div class="col-sm-6">
							<img src="
								<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
								<h4 class="pt-20">
									<a href="#">
										<b>Nama Toko:
											<br/>Nama Produk
										</b>
									</a>
								</h4>
								<ul class="list-li-mr-20 pt-10 mb-30">
									<li>
										<i class="color-primary mr-5 font-12 fa fa-shopping-cart"></i>30
									</li>
									<li>
										<i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47
									</li>
								</ul>
							</div>
							<!-- col-sm-6 -->
							<div class="col-sm-6">
								<img src="
									<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
									<h4 class="pt-20">
										<a href="#">
											<b>Nama Toko:
												<br/>Nama Produk
											</b>
										</a>
									</h4>
									<ul class="list-li-mr-20 pt-10 mb-30">
										<li>
											<i class="color-primary mr-5 font-12 ion-ios-shopping-cart"></i>30
										</li>
										<li>
											<i class="color-primary mr-5 font-12 ion-chatbubbles"></i>47
										</li>
									</ul>
								</div>
								<!-- col-sm-6 -->
							</div>
							<!-- row -->
							<h4 class="p-title mt-20">
								<b>01 REVIEW</b>
							</h4>
							<div class="sided-70 mb-50">
								<div class="s-left rounded">
									<img src="<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
									</div>
									<!-- s-left -->
									<div class="s-right ml-100 ml-xs-85">
										<h5>
											<b>USER NAME, </b>
											<span class="font-8 color-ash">Nov 21, 2017</span>
										</h5>
										<p class="mt-10 mb-15">MANTAP GAN!!!.</p>
									</div>
									<!-- s-right -->
								</div>
								<!-- sided-70 -->
								<h4 class="p-title mt-20">
									<b>LEAVE A REVIEW</b>
								</h4>
								<form class="form-block form-plr-15 form-h-45 form-mb-20 form-brdr-lite-white mb-md-50">
									<input type="text" placeholder="Your Name*:">
										<input type="text" placeholder="Your Email*:">
											<input type="text" placeholder="Your Phone*:">
												<textarea class="ptb-10" placeholder="Your Comment"></textarea>
												<button class="btn-fill-primary plr-30" rows="4" cols="50" type="submit">
													<b>LEAVE A COMMENT</b>
												</button>
											</form>
										</div>
										<!-- col-md-9 -->
										<div class="d-none d-md-block d-lg-none col-md-3"></div>
										<div class="col-md-6 col-lg-4">
											<div class="">
												<h4 class="p-title">
													<b>Popular Produk</b>
												</h4>
												<a class="oflow-hidden pos-relative mb-20 dplay-block" href="#">
													<div class="wh-100x abs-tlr">
														<img src="<?= base_url();?>assets/upload/gambar1547560923.jpg" alt="">
														</div>
														<div class="ml-120 min-h-100x">
															<h5>
																<b>PRODUK</b>
															</h5>
															<h6 class="color-lite-black pt-10">by
																<span class="color-black">
																	<b>Nama Toko</b>
																</span>
															</h6>
														</div>
													</a>
													<!-- oflow-hidden -->
												</div>
												<!-- mtb-50 -->
											</div>
										</div>
									</div>
								</div>
							</section>
>>>>>>> ab219b05f210cc8ac5b5adb5c8a2070fa6dff007:application/views/Detail.php
