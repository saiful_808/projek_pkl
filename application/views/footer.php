
<footer class="bg-191 color-ccc footer_fixed" style="
   bottom:0;
   width:100%;">
  <div class="container">
    <div class="oflow-hidden color-ash font-9 text-sm-center ptb-sm-5">
      <ul class="float-left float-sm-none list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-10">
        <li><a class="pl-0 pl-sm-10" href="#">Terms & Conditions</a></li>
        <li><a href="#">Privacy policy</a></li>
        <li><a href="#">Jobs advertising</a></li>
        <li><a href="#">Contact us</a></li>
      </ul>
      <ul class="float-right float-sm-none list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-5">
        <li><a class="pl-0 pl-sm-10" href="#"><i class="ion-social-facebook"></i></a></li>
        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
        <li><a href="#"><i class="ion-social-google"></i></a></li>
        <li><a href="#"><i class="ion-social-instagram"></i></a></li>
        <li><a href="#"><i class="ion-social-bitcoin"></i></a></li>
      </ul>
    </div><!-- oflow-hidden -->
  </div><!-- container -->
</footer>