
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>admin_assets/login/css/main.css">
</head>
<body style="background-color: #555555;">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<form class="login100-form" method="post">
					<span class="login100-form-title p-b-43">
						Login to continue
					</span>

					<?php
          if($this->session->flashdata('success')) {
             echo '<div class="alert alert-success alert-message">';
             echo $this->session->flashdata('success');
             echo '</div>';
          }
          echo validation_errors('<p style="color:red;>"','</p>')
          ?>

					<div class="validate-input">
            <input type="password" name="pass1" class="wrap-input100 form-control" placeholder="Password Baru">
          </div>

					<div class="validate-input">
            <input type="password" name="pass2" class="wrap-input100 form-control" placeholder="Ketik Ulang Password">
					</div>

					<div class="container-login100-form-btn">
            <button type="submit" name="submit" value="Submit" class="login100-form-btn" style="background-color:#F9B500;">Update Password</button>
            <a href="#" onclick="window.history.go(-1)" class="btn btn-default">Kembali</a>

        </form>
      </div>
				<div class="login100-more" style="background-image: url('admin_assets/login/images/bg-01.jpg')">
				</div>
			</div>
		</div>
	</div>
  <script src="<?= base_url(); ?>admin_assets/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>admin_assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url();?>admin_assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?= base_url();?>admin_assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url();?>admin_assets/login/js/main.js"></script>

</body>
</html>
