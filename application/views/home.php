<style>
.top{
  background-color: #faf9f5;
  padding: 20px;
}
.product.product-single {
  margin-top: 15px;
  margin-bottom: 15px;
  -webkit-transition: 0.3s all;
  transition: 0.3s all;
}

.product.product-single:hover {
  -webkit-box-shadow: 0px 6px 10px -6px rgba(0, 0, 0, 0.175);
  box-shadow: 0px 6px 10px -6px rgba(0, 0, 0, 0.175);
  -webkit-transform: translateY(-4px);
  -ms-transform: translateY(-4px);
  transform: translateY(-4px);
}

.product.product-single .product-btns {
  margin-top: 20px;
  opacity: 0;
  visibility: hidden;
  -webkit-transition: 0.3s all;
  transition: 0.3s all;
}

.product.product-single .product-btns .tombol{
  background-color: #F9B500;
}
.product.product-single:hover .product-btns {
  opacity: 1;
  visibility: visible;
}

.tombol{
  background-color: #F9B500;
}
  .bottom-side {
    margin-top: -60px;
    height: 60px;
    z-index: 5;
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 10px;
    text-align: center;
    line-height: 60px;
    color: white;
    font-size: 25px;
    border-radius: 10px;
  }

  .card {
    border-radius: 10px;
    border: none;
  }
  .card h3 {
    font-weight: bold;
  }
  .card-title img {
    width: 30px;
  }
  .top-head {
    position: relative;
  }
  .top_tiles {
    margin-left: 10px;
  }
  .right-side img {
    z-index: 0;
    border-radius: 10px;
  }

  @media(min-width:800px) {
    .card-title img {
      width: 65px;
    }
    .cari select{
      height:30px;
      border-radius:5px;
    }
    .cari{
      margin-right: 130px;
      float:right;
      padding: 10px;
      /*background-color: pink;*/
    }
    .cari button[type = "submit"]{
        background-color: #F9B500;
        border-radius: 5px;
        height:24px;
        padding-left: 5px;
         padding-right: 5px;
    }

    .cari button[type = "submit"]:hover {
        background-color: #Fff;
        border-radius: 5px;
        height: 24px;
    }
    .card {
      width: 15%;
      margin: 5px;
      margin-left: 20px;
      border-radius: 10px;
      border: none;
    }

    .card h3 {
      font-size: 15px;
    }

    .top-head {
      position: relative;
      margin-left: 7.5%;
    }

    .top_tiles {
      float: left;
      margin-left: 30px;
    }

    .right-side {
      height: 265px;
      padding: 0;
      margin-left: -30px;
    }

    .right-side img {
      z-index: 0;
      border-radius: 10px;
    }

    .bottom-side {
      margin-top: -60px;
      margin-left: -30px;
      height: 60px;
      z-index: 5;
      background-color: rgba(0, 0, 0, 0.5);
      border-radius: 10px;
      text-align: center;
      line-height: 60px;
      color: white;
      font-size: 25px;
    }
  }

  @media(max-width:600px) {
    .card-title img {
      width: 60px;
    }

    .card h3 {
      font-size: 16px;
      font-weight: bold;
    }
  }
</style>
<div class="cari" style="">
  <?php echo form_open('index.php/Shop/cari');?>
  <select name="cariberdasarkan" id="cariberdasarkan" >
    <option value="">Cari Bardasarkan</option>
    <option value="Id">Name Toko</option>
    <option value="Join_Since">Join Since</option>
  </select>
  <input type="text"  placeholder="Search here" name="yangdicari">
    <button type="submit" class="" >
      <i class="ion-search"></i> Search
    </button>
    <?php echo form_close();?>
  </div>
  <div style="clear: both;"></div>
  <br>
    <div class="top-head">
      <div class="row top_tiles">
        <div class="col-lg-5 col-md-2 col-sm-5 col-6 card">
          <div class=" card-body">
            <div class="card-title">
              <img src="
                <?= base_url() ?>assets/upload/icon/revenue.png">
              </div>
              <div class="card-text">
                <b>$ </b>
                <?php echo number_format("$link->total_revenue",2,",",".");?>
              </div>
              <h3>REVENUE</h3>
            </div>
          </div>
          <div class="col-lg-5 col-md-2 col-sm-5 col-6 card">
            <div class="card-body">
              <div class="card-title">
                <img src="
                  <?= base_url() ?>assets/upload/icon/shopping.png">
                </div>
                <div class="card-text">
                  <?= $link->total_sales;?>
                  <b> pcs</b>
                </div>
                <h3>SALES</h3>
              </div>
            </div>
            <div class="col-lg-5 col-md-2 col-sm-5 col-6 card">
              <div class="card-body">
                <div class="card-title">
                  <img src="
                    <?= base_url() ?>assets/upload/icon/calendar.png">
                  </div>
                  <div class="card-text">
                    <?= $link->Join_since;?>
                  </div>
                  <h3>OPEN</h3>
                </div>
              </div>
              <div class="col-lg-5 col-md-2 col-sm-5 col-6 card">
                <div class="card-body">
                  <div class="card-title">
                    <img src="
                      <?= base_url() ?>assets/upload/icon/good-review.png">
                    </div>
                    <div class="card-text"><?php 
                    if ($rating->rating == "") {
                      echo(0);
                    }else {
                      echo($rating->rating);
                    } ?></div>
                    <h3>REVIEW</h3>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-10 right-side">
                  <img src="
                    <?= $link->Thumbnail;?>" height="265">
                  </div>
                  <div class="col-lg-10 bottom-side">
                    <?= $link->Id;?>
                  </div>
                </div>
              </div>
              <div class="container" style="clear: both;"></div>
              <div class="top">
              <div class="container">
                <h4 class="p-title mt-50">
                  <b>TOP 20 Shops</b>
                </h4>
                <div class="row">
                  <!-- 20 toko -->
                 <?php foreach ($data as $dta) :?>
                    <div class="  col-md-3 col-sm-6 col-xs-6">
                    <div class="card product product-single" style="width: 16rem;">
                    <img src=" <?= $dta->Thumbnail; ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">  <?= $dta->Id;?></h5>
                      <p class="card-text"> Total Revenue:<br> <b>$ </b>
                           <?php echo number_format("$dta->total_revenue",2,",",".")?></p>
                      <p class="card-text"><i class="color-primary font-12 fas fa-shopping-bag"></i>Qty
                        <?= $dta->total_produk;?>
                        <i class="color-primary font-12 fas fa-shopping-cart"></i>Sold <?= $dta->total_sales;?></p>
                      <div class="product-btns">
                         <a href="<?php echo site_url()."/Shop/detail/".$dta->Id;?>" class="btn tombol"><i class="fa fa-search-plus"></i> view</a>
                    </div>
                    </div>
                  </div>
                  </div>
                      <?php endforeach;?> 
                    </div>
                    <!-- End 20 toko -->
                  </div>
                  </div>
                  <!-- all shop -->
                  <div class="container">
                    <h4 class="p-title mt-50">
                      <i class="fas fa-list-ul"></i>
                      <b>Daftar Semua Toko</b>
                    </h4>
                  </div>
                  <!-- jadi -->
                  <!-- <h2 style="margin-left:80px;">Welcome back
                  <?= $this->session->userdata('ses_nama');?></h2> -->
                    <div class="container">
                      <table id="datatable-buttons" class="table table-hover table-bordered dt-responsive nowrap">
                        <div class="clearfix"></div>
                        <div class="container">
                          <br >
                            <br >
                              <!-- Page Heading -->
                              <div class="row">
                                <table class="table table-striped" id="mydata">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Name</th>
                                      <th>Join since</th>
                                      <th>Detail</th>
                                    </tr>
                                  </thead>
                                  <tbody id="show_data"></tbody>
                                </table>
                                <div class="pagination" style="float:right;margin-top:auto;margin-right:20px;"></div>
                              </div>
                            </div>
                          </div>
