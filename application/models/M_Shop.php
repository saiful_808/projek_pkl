<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Shop extends CI_Model {

    function __construct(){
    }

    function cek_login()
    {
        if ($this->session->userdata('login') == FALSE) {
            redirect('login');
        }
    }

    function insert($table = '', $data = '')
    {
        $this->db->insert($table, $data);
    }

   function insert_last($table = '', $data = '')
   {
    $this->db->insert($table, $data);

	  return $this->db->insert_id();
   }

    function get_all($table)
    {
        $this->db->from($table);

        return $this->db->get();
    }


    function get_where($table = null, $where = null)
    {
    	$this->db->from($table);
    	$this->db->where($where);

  		return $this->db->get();
  	}


	function get_where_admin($table)
	{
		$this->db->from($table);
		$this->db->where('level',2);
		return $this->db->get();
  }

  function get_where_user($table)
  {
    $this->db->from($table);
    $this->db->where('level',1);
    return $this->db->get();
  }

    function update($table = null, $data = null, $where = null)
    {
        $this->db->update($table, $data, $where);
    }

    function update_pass($data){
        $password    = $data['password'];
        $reset = $data['reset'];
        return  $this->db->query("update user_web set password='$password' where reset='$reset'");
    }

    function update_reset($data){
        $email    = $data['email'];
        $reset = $data['reset'];
        return  $this->db->query("update user_web set reset='$reset' where email='$email'");
    }

    function delete($table = null, $where = null)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
     function data_list(){
        $hasil=$this->db->get('daftar_toko');
        return $hasil->result();
    }
    function cari($cariberdasarkan,$yangdicari){
        $this->db->from("daftar_toko");
        switch ($cariberdasarkan){
            case "":
            $this->db->like('Id',$yangdicari);
            $this->db->or_like('Join_since',$yangdicari);
            break;
            case "Join_since":
            $this->db->where('Join_since',$yangdicari);
            break;
            default:
            $this->db->like($cariberdasarkan,$yangdicari);
            break;
        }
        return $this->db->get();

        }
}
