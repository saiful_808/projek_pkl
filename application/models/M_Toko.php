
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Toko extends CI_Model{
    function data_toko(){
        $hasil=$this->db->get('daftar_toko');
        return $hasil->result();
    }
      function top(){
         $this->db->from('daftar_toko');
         $this->db->select('daftar_toko.Id,daftar_toko.Join_since,daftar_toko.Thumbnail');
         // $this->db->select('sum(review.rating) as rating ');
         $this->db->select('TRUNCATE(SUM(produk.sold*produk.price),2) as total_revenue');
         $this->db->select('SUM(produk.sold) as total_sales');
         // $this->db->select('count(review.shop) as hasil');
         // $this->db->join('review','daftar_toko.Id = review.shop','left');
         $this->db->join('produk','daftar_toko.Id  = produk.id_toko');
         $this->db->group_by(array('produk.id_toko'));
         $this->db->order_by('total_revenue',' DESC');
        return $this->db->get()->row();
    }

    function rating($kunci){
        $this->db->where('shop',$kunci);
        $this->db->select('SUM(rating) as rating');
        return $this->db->get('review')->row();
    }
    function twenty(){
        $this->db->from('daftar_toko');
        $this->db->select('daftar_toko.Id,daftar_toko.Thumbnail,daftar_toko.No');
        $this->db->select('TRUNCATE(SUM(produk.sold*produk.price),2) as total_revenue');
        $this->db->select('SUM(produk.sold) as total_sales');
        $this->db->select('count(produk.id_toko) as total_produk');
        $this->db->join('produk','daftar_toko.Id  = produk.id_toko');
        $this->db->group_by(array('produk.id_toko'));
        $this->db->order_by('total_revenue',' DESC');
        $this->db->limit(20);
        return $this->db->get()->result();
    }
    function all_data($id){
         $this->db->from('daftar_toko');
         $this->db->select('daftar_toko.Id,daftar_toko.Join_since,daftar_toko.Thumbnail');
         $this->db->select('count(review.shop) as hasil');
         $this->db->select('count(id_toko) as total_produk');
         $this->db->select('SUM(produk.sold) as total_sales');
         $this->db->select('TRUNCATE(SUM(produk.sold*produk.price),2) as total_revenue');
         $this->db->join('review','daftar_toko.Id = review.shop','left');
         $this->db->join('produk','daftar_toko.Id  = produk.id_toko')->where('daftar_toko.Id',$id);
         $this->db->group_by(array('produk.id_toko'));
         return $this->db->get()->row();
    }
    function rating_p($id){
        $this->db->where('shop',$id);
        $this->db->SELECT("count(shop) as hasil");
        return $this->db->get('review')->row_array();
    }
    function produk($id){
        $this->db->from('produk');
        $this->db->select('title,thumbnail,link,price,id,sold');
        $this->db->select('count(review.coment) as coment');
        $this->db->select('SUM(review.rating)as rating');
        $this->db->select('SUM(sold) as total_sales');
        $this->db->select('TRUNCATE(SUM(sold * price),2)as total_revenue_p');
        $this->db->join('review','produk.id=review.product_id','left');   
        $this->db->where('id_toko',$id);
        $this->db->group_by(array('id'));
        $this->db->order_by('total_revenue_p','DESC');
        // $this->db->limit(10);
        return $this->db->get()->result();
    }
    function count($id){
        $this->db->where('id_toko',$id);
        return $this->db->get('produk')->num_rows();
    }
    //coment per produk
    function coment($id){
        $this->db->from('produk');
        // $this->db->select('produk.title,produk.id,produk.link,produk.sold,produk.price,produk.thumbnail');
        // $this->db->select('count(review.coment) as total_coment');
        // $this->db->select('SUM(review.rating)as rating');
        $this->db->select('TRUNCATE(SUM(produk.sold * produk.price),2)as total_revenue_p');

        // $this->db->join('review','produk.id=review.product_id')->where('product_id',$id);
        $this->db->group_by(array('produk.id'));
        return $this->db->get()->result();
    }
    function dt_barang($id){
        $this->db->from('produk');
        $this->db->where('id',$id);
        $this->db->select('produk.title,produk.id,produk.link,produk.sold,produk.price,produk.thumbnail');
        $this->db->select('TRUNCATE(SUM(produk.sold * produk.price),2)as total_revenue_p');
        $this->db->select('SUM(produk.sold) as total_sales');
        $this->db->select('count(review.coment) as total_coment');
        $this->db->select('SUM(review.rating)as rating');
        $this->db->join('review','produk.id=review.product_id','left');
        $this->db->limit(10);
        return  $this->db->get()->row();
    }
    function coment_dt($id){
        $this->db->where('product_id',$id);
        return $this->db->get('review')->result();
    }
}

